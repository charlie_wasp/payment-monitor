<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Apprentice_List_Table
 *
 * @author root
 */

class Apprentice_List_Table extends WP_List_Table {

   /**
    * Constructor, we override the parent to pass our own arguments
    * We usually focus on three parameters: singular and plural labels, as well as whether the class supports AJAX.
    */
    function __construct() {
        parent::__construct( array(
            'singular' => 'wp_apprentice', //Singular label
            'plural'   => 'wp_apprentices', //plural label, also this well be one of the table css class
            'ajax'     => false,
            'screen'   => get_current_screen()//We won't support Ajax for this table
        ) );
    }
    
    function extra_tablenav($which) {
        if ($which == "top") { ?>
            <button class="button create-user-button button-primary">Добавить</button>
            <button class="button delete-user-button button-primary">Удалить</button>
            <button class="button edit-user-button button-primary">Редактировать</button>
<?php  }
        if ($which == "bottom") {
            //The code that goes after the table is there
            //echo"Hi, I'm after the table";
        }
    }
    
    /**
     * Define the columns that are going to be used in the table
     * @return array $columns, the array of columns to use with the table
     */
    function get_columns() {
        return $columns = array(
            'cb' => '<input type="checkbox">',
            'rownum' => '#',
            'col_student_name' => __('Name'),
            'col_student_contacts' => 'Контакты',
            'col_student_status' => __('Status')
        );
    }
    
    function column_cb($item) {
        return sprintf(
            '<input form="delete-user-form" type="checkbox" name="users[]" value="%s" />', $item->ID
        );
    }
    
//    function process_bulk_action() {
////        print '<pre>' . print_r( $_POST, true) . '</pre>';
////        exit;
//        //Detect when a bulk action is being triggered...
//        if ('delete' === $this->current_action()) {
//            $users_id = filter_input(INPUT_POST, 'users', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
//            
//            foreach ($users_id as $user_id) {
//                wp_delete_user($user_id);
//            }
//        }
//    }

    /**
     * Decide which columns to activate the sorting functionality on
     * @return array $sortable, the array of columns that can be sorted by the user
     */
    public function get_sortable_columns() {
        return $sortable = array(
            'col_student_name' => 'display_name',
        );
    }
    
    /**
     * Prepare the table with different parameters, pagination, columns and table elements
     */
    function prepare_items() {
        global $wpdb;
        global $payment_monitor_table_name;
        
//        $this->process_bulk_action();
        
        $query_args = array( 'role' => 'apprentice' );
        $user_query = new WP_User_Query( $query_args );
        
        /* -- Ordering parameters -- */
        //Parameters that are going to be used to order the result
        $orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : 'ASC';
        $order = !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : '';
        
        if (!empty($orderby) & !empty($order)) {
            $query_args['orderby'] = $orderby;
            $query_args['order'] = $order;
        }
        
        /* -- Pagination parameters -- */
        //Number of elements in your table?
        $totalitems = $user_query->get_total(); //return the total number of affected rows
        //How many to display per page?
        $perpage = 20;
        //Which page is this?
        $paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';
        //Page Number
        if (empty($paged) || !is_numeric($paged) || $paged <= 0) {
            $paged = 1;
        }
        //How many pages do we have in total?
        $totalpages = ceil($totalitems / $perpage);
        //adjust the query to take pagination into account
        if (!empty($paged) && !empty($perpage)) {
            $offset = ($paged - 1) * $perpage;
            $query_args['offset'] = $offset;
            $query_args['number'] = $perpage;
        }
        
        $user_query = new WP_User_Query( $query_args );
        
        /* -- Register the pagination -- */
        $this->set_pagination_args(array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ));
        
        //The pagination links are automatically built according to those parameters

        /* -- Register the Columns -- */
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        /* -- Fetch the items -- */
        $this->items = $user_query->get_results();
    }
    
//    function get_bulk_actions() {
//        $actions = array(
//            'delete' => 'Удалить',
//            'edit' => 'Изменить'
//        );
//        
//        return $actions;
//    }

    /**
     * Display the rows of records in the table
     * @return string, echo the markup of the rows
     */
    function display_rows() {

        //Get the records registered in the prepare_items method
        $records = $this->items;

        //Get the columns registered in the get_columns and get_sortable_columns methods
        list( $columns, $hidden ) = $this->get_column_info();
        //print '<pre>' . print_r($columns, true) . '</pre>';
        //exit;
        //Loop for each record
        if (!empty($records)) {
            foreach ($records as $i => $rec) {
                $status_obj = payment_monitor_get_user_status($rec->ID);
                $row_attr = "data-uid='{$rec->ID}' id='record_{$rec->ID}'";
                $row_attr .= " data-login='{$rec->user_login}'";
                if (!is_null($status_obj)) {
                    $row_attr .= " data-bill-id='{$status_obj->last_bill_id}'";
                }
                //Open the line?>
                <tr <?php print $row_attr;?>>
                <?php
                foreach ($columns as $column_name => $column_display_name) {

                    //Style attributes for each col
                    $class = "class='$column_name column-$column_name'";
                    $style = "";
                    if (in_array($column_name, $hidden))
                        $style = ' style="display:none;"';
                    $attributes = $class . $style;

                    //edit link
                    //$editlink = '/wp-admin/link.php?action=edit&link_id=' . (int) $rec->link_id;

                    //Display the cell
                    switch ($column_name) {
                        case "rownum":?>
                        <th>
                            <?php 
                            $paged = filter_input(INPUT_GET, 'paged', FILTER_VALIDATE_INT);
                            
                            $modifier = !empty($paged) ? ($paged - 1) * 20 : 0;
                            
                            print $i + 1 + $modifier; 
                            ?>
                        </th>
                    <?php break;
                        case "cb":?>
                            <th scope="row" class="check-column">
                                <?php print $this->column_cb($rec); ?>
                            </th>
                    <?php break;
                        case "col_student_name":?>
                            <td <?php print $attributes; ?>>
                                <a href="#" class="edit-user-link">
                                    <?php print $rec->display_name;?>
                                </a>
                            </td>
                    <?php  break;
                        case "col_student_contacts": ?>
                            <td <?php print $attributes; ?>>
                                <a href="mailto:<?php print $rec->user_email; ?>">
                                    <?php print $rec->user_email; ?>
                                </a>,
                                <a href="skype:<?php print get_user_meta($rec->ID, 'skype', true);?>">
                                    <?php print get_user_meta($rec->ID, 'skype', true);?>
                                </a>
                            </td>
                    <?php  break;
                        case "col_student_status": ?>
                            <td <?php print $attributes;?> data-status="<?php print $status_obj->status ?>">
                                <?php $this->render_status($status_obj); ?>
                            </td>
                    <?php  break;
                    }
                }

                //Close the line
                echo'</tr>';
            }
        }
    }
    
    function render_status($status_obj) {
        switch ($status_obj->status) {
            case null: ?> 
            <p>Квитанций нет</p>                
     <?php break;
            case BILL_IDLE: ?>
            <div class="payment-accept-datepicker"></div>
            <a href="#" class="status-display button view-bill btn-warning btn-xs" data-toggle="modal"
               title="Нажмите, чтобы посмотреть квитанцию">
                На проверке
            </a>
            <button class="status-buttons button payment-accept label-success">
                <span class="dashicons dashicons-yes"></span>
            </button>
            
            <button class="status-buttons button payment-reject label-danger">
                <span class="dashicons dashicons-no-alt"></span>
            </button>
            
        <?php break;
            case BILL_REJECTED:
            case BILL_EXPIRED: ?>
            <span class="status-display label label-danger">Не действительно</span>
        <?php break;
            case BILL_PAID: ?>
            <span class="status-display label label-success">до <?php print $status_obj->expire;?></span>
        <?php break;
        }
    }

}