(function($) {
    $(document).ready(function(){
        //$('.payment-accept-datepicker').css('position', 'absolute');
            
        $('body').on('click', '.ui-datepicker-header', function(e) {
            e.preventDefault();
            e.stopPropagation();
        });
        
        $('.payment-accept').click(function(e){
            e.stopPropagation();
            e.preventDefault();
            
            $(this).siblings('.payment-accept-datepicker').first()
                    .css('position', 'absolute').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                selectOtherMonths: false,
                showOn: 'both',
                onSelect: function(dateText) {
                    var row = $(this).parents('tr').eq(0);

                    $(document.createElement('input')).attr({
                        type: 'hidden',
                        name: 'Bill[expire]',
                        value: dateText
                    }).appendTo('#payment-change-status-form');

                    $(document.createElement('input')).attr({
                        type: 'hidden',
                        name: 'Bill[id]',
                        value: row.data('bill-id')
                    }).appendTo('#payment-change-status-form');

                    $('#payment-change-status-form').find('[name=status]').val(BillStatus.paid);
                    
                    $('#payment-change-status-form').submit();
                    //console.log(dateText);
                }
            }).show(); 
        });
        
        $('.col_student_status .payment-reject').click(function(event) {
            event.stopPropagation();
            event.preventDefault();
            
            var row = $(this).parents('tr').eq(0);
            
            $(document.createElement('input')).attr({
                type: 'hidden',
                name: 'bill_id',
                value: row.data('bill-id')
            }).appendTo('#payment-change-status-form');
            
            $('#payment-change-status-form').find('[name=status]').val(BillStatus.rejected);
            
            $('#payment-change-status-form').submit();
        });
        
        $(document).click(function(event) {
            var elem = $(event.target);
            if (!elem.hasClass("hasDatepicker") &&
                !elem.hasClass("ui-datepicker") &&
                !elem.hasClass("ui-icon") &&
                !elem.hasClass("ui-datepicker-next") &&
                !elem.hasClass("ui-datepicker-prev") &&
                !$(elem).parents(".ui-datepicker").length) {
                $('.payment-accept-datepicker').datepicker('destroy');
            }
        });
        
        $(".payment-accept-datepicker").click(function(e) {
            e.stopPropagation(); // This is the preferred method.
        });
        
        $('.wrap').on('click', '.view-bill', function(event) {
            event.preventDefault();
            var billId = $(this).parents('tr').data('bill-id');
            var action = 'fetch-bill-url';
       
            $.ajax({
                type:'GET',
                data:{
                    action: action,
                    bill_id: billId
                },
                dataType: 'text',
                url: "/wp-admin/admin-ajax.php",
                success: function(value) {
                    $('#view-bill')
                            .html('<img class="img-responsive" src="'+value+'">')
                            .dialog({
                                autoOpen: true,
                                height: 500,
                                width: 500
                            });
                }
            });
        });
        
        $('#payment-change-status-form').on('submit', function(event) {
           if ($(this).find('[name="Bill[status]"]').val() === BillStatus.rejected 
                && !confirm('Вы действительно хотите отменить оплату?')) {
               event.preventDefault();
           } 
        });
        
        $('.delete-user-button').on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            
            var user_names = [];
               $('[name="users[]"]:checked').each(function(i, el) {
                  user_names.push( $(el).parents('tr').find('.col_student_name').text().trim() ); 
               });
               var confirmed = confirm("Следующие пользователи будут удалены:\n\n"
                       +user_names.join(', ')+".\n\n Продолжить?");
               
               if (confirmed) {
                   $('#delete-user-form').submit();
               }
        });
        
        $('.edit-user-link').on('click', function(event) {
           $(this).parents('tr').find('[type=checkbox]').prop('checked', true);
           editUserFormOpen(event);
           $(this).parents('tr').find('[type=checkbox]').prop('checked', false);
        });
        
        $('.edit-user-button').on('click', editUserFormOpen);
        
        $('.create-user-button').on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            
            $('#user_form input[name="User[ID]"]').remove();
            $('#user_form .status-buttons').hide();
            $('#user_form input[name="Bill[expire]"]').prop('disabled', true).hide();
            $('#user_form input[type=text]').val('');
            $('#user_form [type=submit]').val('Добавить нового пользователя');
                
            $('#user_form').slideDown();
        });
        
        $('#user_form .status-buttons').on('click', function(event){
            if ( !$(this).hasClass('view-bill') ) {
                event.preventDefault();
                event.stopPropagation();
            }
            
            if ( $(this).hasClass('payment-reject') ) {
                $('#user_form [name="Bill[status]"]').val(BillStatus.rejected);
                 $('.current-status > .status-display', '#user_form')
                        .removeClass('label-success label-warning')
                        .addClass('label-danger')
                        .text('Недействительно');
                $('#user_form [name="Bill[expire]"]').val('');
            } 
            if ( $(this).hasClass('payment-accept') ) {
                var expire_date = $('#user_form [name="Bill[expire]"]').val();
                
                if ( expire_date === '' ) {
                    alert('Выберите, до какого срока действительная квитанция');
                    return;
                }
                
                if ( !/\d{4}-\d{2}-\d{2}/.test(expire_date) ) {
                    alert('Срок квитанции должен быть в формате ГГГГ-ММ-ДД');
                    return;
                }
                
                $('#user_form [name="Bill[status]"]').val(BillStatus.paid);
                
                $('.current-status > .status-display', '#user_form')
                        .removeClass('label-danger label-warning')
                        .addClass('label-success')
                        .text('до '+expire_date);
            }
        });
        
        $('#user_form .button-cancel').on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            
            $('#user_form input[name="User[ID]"]').remove();
            
            $('#user_form').slideUp(500, function() {
                $('#user_form input[type=text]').val('');
                $('#user_form [type=submit]').val('Добавить нового пользователя');
                $('.form-payment-accept-datepicker', '#user_form').datepicker('destroy');
            });
            
        });
        
        $('.show-password').on('click', function(event) {
           event.preventDefault();
           event.stopPropagation();            
           
           var passwordInput = $(this).siblings('input').eq(0); 
           if (passwordInput.attr('type') === 'password') {
               passwordInput.attr('type', 'text');
           } else {
               passwordInput.attr('type', 'password');
           }
        });
        
        $('.generate-password').on('click', function() {
           event.preventDefault();
           event.stopPropagation();
           
           var randomstring = Math.random().toString(36).slice(-8);
           $(this).siblings('input').eq(0).val(randomstring); 
        });
    });
    
function editUserFormOpen(event) {
    event.preventDefault();
    event.stopPropagation();
    
    $('body, html').animate({scrollTop: 0}, 500);
    $('#user_form').slideDown();

    var edit_row = $('[name="users[]"]:checked').first().parents('tr'),
        uid = $(edit_row).data('uid'),
        ulogin = $(edit_row).data('login'),
        status = $('.col_student_status', edit_row).data('status'),
        bill_id = $(edit_row).data('bill-id');

    $('#user_form').append('<input type="hidden" name="User[ID]" value='+uid+'>');
    $('#user_login').val(ulogin);

    var user_nice_name = $(edit_row).find('.col_student_name').text().trim().split(' ');

    $('#first_name').val(user_nice_name[0]);
    $('#last_name').val(user_nice_name[1]);

    var user_contacts = $(edit_row).find('.col_student_contacts').text().split(',');

    $('#email').val(user_contacts[0].trim());
    $('#skype').val(user_contacts[1].trim());
    
    $('#user_form [name="Bill[status]"]').val(status);
    $('.bill-section tr', '#user_form').attr('data-bill-id', bill_id);
    
    if (status !== BillStatus.idle) {
        $('.col_student_status', edit_row).find('.status-display')
            .first().clone(true).appendTo('#user_form .current-status');
    } else {
        $('#user_form .current-status').append('<span class="label status-display label-warning">На проверке</span>')
    }
    
    $('#user_form').append('<input type="hidden" name="Bill[id]" value="'+bill_id+'" />');
    
    $('.form-payment-accept-datepicker', '#user_form').datepicker({dateFormat: 'yy-mm-dd'});
    
    $('#user_form .status-buttons').show();
    $('#user_form input[name="Bill[expire]"]').prop('disabled', false).show();
            
    $('#user_form [type=submit]').val('Сохранить изменения');
}    
}(jQuery));


