$(document).ready(function() {
   $('#generatePassword').on('click', function() {
      var randomstring = Math.random().toString(36).slice(-8);
      
      $('#inputPassword').val(randomstring);
      $('#inputConfirmPassword').val(randomstring);
   }); 
   
   //Check for password confirmation
   $('#edit-user-info-form').on('submit', function(e) {
        if ( $('#inputPassword').val() !== $('#inputConfirmPassword').val() ) {
            //TODO Add notification
            console.log('Пароли не совпадают');
            e.preventDefault();
        }
        
        if ( !isEmailValid( $('#inputEmail').val() ) ) {
            console.log('E-mail невалиден');
            //TODO Add notification here too
            e.preventDefault();
        }
   });
   
   $('#bills-list').on('click', '.view-bill-link', function() {
        var billId = $(this).parents('tr').data('bill-id');
        var action = 'fetch-bill-url';
       
        $.ajax({
            type:'GET',
            data:{
                action: action,
                bill_id: billId
            },
            dataType: 'text',
            url: "/wp-admin/admin-ajax.php",
            success: function(value) {
                $('#modalPayment .modal-body').html('<img class="img-responsive" src="'+value+'">');
            }
        });
   });
   
    $('#confirm-delete').on('show.bs.modal', function(e) {
        console.log('Confirm delete');
        if ($(e.relatedTarget).hasClass('remove-bill-link')) {
            var relCheckbox = $(e.relatedTarget).parent('td').siblings()
                                             .find('input[type=checkbox]').eq(0);
            //remove-bill-form-submit remove-bill-link
            $(this).find('.danger').on('click', function() {
                relCheckbox.prop('checked', true);
                $('#'+relCheckbox.attr('form')).submit();
            });
        } else {
            var removeBillForm = $(e.relatedTarget).parent();
            
            $(this).find('.danger').on('click', function() {
                removeBillForm.submit(); 
            }); 
        }
    });
    
    $('.look-password').on('click', function() {
        var passwordInputs = $('[name=user_pass], [name=confirm-password]');
        if (passwordInputs.attr('type') === 'password') {
            passwordInputs.attr('type', 'text');
            $(this).children('.glyphicon').removeClass('glyphicon-eye-open');
            $(this).children('.glyphicon').addClass('glyphicon-eye-close');
        } else {
            passwordInputs.attr('type', 'password');
            $(this).children('.glyphicon').removeClass('glyphicon-eye-close');
            $(this).children('.glyphicon').addClass('glyphicon-eye-open');
        }    
    });
    
    $('.widget_load_bill_widget').on('change', '[name=bill-image]', function() {
        $(this).siblings('[type=submit]').prop('disabled', $(this).val() == '');
    });
});

function isEmailValid(email) {
    console.log(email)
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 


