<?php
/*
Plugin Name: Payment Monitor
Description: A plugin for monitoring payments by verifying photos with checks
Version: 0.9c
Author: Ramazanov Timur
License: MIT
*/

global $payment_monitor_db_version;

global $payment_monitor_table_name;

$payment_monitor_table_name = 'pay_bills';

$payment_monitor_db_version = "0.1";

require_once dirname(__FILE__) . '/Load_Bill_Widget.php';

define('BILL_PAID', 'bill_paid');
define('BILL_IDLE', 'bill_idle');
define('BILL_EXPIRED', 'bill_expired');
define('BILL_REJECTED', 'bill_rejected');

register_activation_hook( __FILE__, 'payment_monitor_install' );
//register_deactivation_hook( __FILE__, 'payment_monitor_uninstall' );

//function payment_monitor_userfields($contactmethods) {
//    //Adds customer contact details
//    $contactmethods['skype1'] = 'Skype';
//    return $contactmethods;
//}

add_action('admin_print_scripts-user-new.php', 'add_script');
add_action('admin_post_submit-form', '_handle_form_action');
add_action('admin_post_upload-bill', 'payment_monitor_upload_bill');
add_action('admin_post_remove-bill', 'payment_monitor_remove_bill');
add_action('admin_post_change-bill-status', 'payment_monitor_change_bill_status');
add_action('admin_post_create-user', 'payment_monitor_create_user');
add_action('admin_post_delete-user', 'payment_monitor_delete_user');

add_action('wp_ajax_fetch-bill-url', 'payment_monitor_fetch_bill_url');

add_action( 'delete_user', 'payment_monitor_on_user_delete' );

add_action( 'admin_menu', 'payment_monitor_menu' );
add_action( 'admin_notices', 'payment_monitor_admin_notice' );

//add_action('register_form', 'payment_monitor_extra_profile_fields');
add_action( 'widgets_init', 'payment_monitor_load_widget' );

function _handle_form_action() {
    if (!session_id()) {
        session_start();
    }
    
    $was_error = false;
    
    $_SESSION['payment_monitor_messages'] = array();
            
    $uid = filter_input(INPUT_POST, 'ID', FILTER_SANITIZE_NUMBER_INT);
    
    unset($_POST['action'], $_POST['confirm-password']);
    
    $skype = filter_input(INPUT_POST, 'skype', FILTER_SANITIZE_STRING);
    unset($_POST['skype']);
     
    if (get_user_meta($uid, 'skype', true) != $skype) {
        $was_error = $was_error || (update_user_meta($uid, 'skype', $skype) === false);
        if ($was_error) {
            _log('Error in payment-monitor plugin in ' . __FILE__ . ' file '
                    . 'while changing skype meta field');
        }
    }
    
    if ( empty($_POST['user_pass']) ) {
        unset($_POST['user_pass']);
    }
    
    $user_data = filter_input_array(INPUT_POST, array(
        'display_name' => FILTER_SANITIZE_STRING,
        'ID'           => FILTER_SANITIZE_NUMBER_INT,
        'user_email'   => FILTER_VALIDATE_EMAIL
    ));
    
    $update_res = wp_update_user($user_data);
    
    $was_error = $was_error || is_wp_error( $update_res );
    
    if ($was_error) {
        $_SESSION['payment_monitor_messages']['danger'] = 'При сохранении данных произошла ошибка';
        _log('Error in payment-monitor plugin in ' . __FILE__ . ' file - ' . print_r($update_res, true));
    } else {
        $_SESSION['payment_monitor_messages']['success'] = 'Данные успешно сохранены';
    }
    
    $location = $_SERVER['HTTP_REFERER'];
    wp_safe_redirect($location);
}

function add_script() {

    wp_enqueue_script(
        'add_input_field',
        plugins_url('add-user-fields.js', __FILE__),
        array('jquery'),
        false,
        true
    );
}

add_filter('user_contactmethods', 'payment_monitor_userfields');

function payment_monitor_install() {
    global $wpdb;
    global $payment_monitor_db_version;
    global $payment_monitor_table_name;
    
    $sql = "CREATE TABLE {$wpdb->prefix}$payment_monitor_table_name (
        id int(11) NOT NULL AUTO_INCREMENT,
        upload_time datetime NOT NULL,
        status varchar(25) NOT NULL default '".BILL_IDLE."',
        verification_date datetime NULL,
        expiration_date date NULL,
        uid bigint(20) NOT NULL,
        file_path varchar(255) NOT NULL,
        file_url varchar(255) NOT NULL,
        UNIQUE KEY id (id)
    );";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($sql);
    
    //add_option( "payment_monitor_db_version", $payment_monitor_db_version );
    
    add_role(
        'apprentice', __('Apprentice'), array(
            'read' => true, // true allows this capability
            'load_bills' => true,
        )
    );
    
    if (!file_exists(ABSPATH . 'wp-content/uploads/students')) {
        if (file_exists(ABSPATH . 'wp-content/uploads/')) {
            mkdir(ABSPATH . 'wp-content/uploads/students');
        } else {
            mkdir(ABSPATH . 'wp-content/uploads');
            mkdir(ABSPATH . 'wp-content/uploads/students');
        }
    }
}

function payment_monitor_uninstall() {
    
}

// Register and load the widget
function payment_monitor_load_widget() {
    register_widget( 'load_bill_widget' );
}

if (!function_exists('_log')) {

    function _log($message) {
        if (WP_DEBUG === true) {
            if (is_array($message) || is_object($message)) {
                error_log(print_r($message, true));
            } else {
                error_log($message);
            }
        }
    }

}

function payment_monitor_get_user_bills($user_id) {
    global $wpdb;
    global $payment_monitor_table_name;
    
    $bills = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}$payment_monitor_table_name "
            . "WHERE uid = $user_id ORDER BY upload_time DESC", OBJECT);
    
    $out = array();
    
    if ( !is_null($bills) ){
        foreach ($bills as $bill) {
            $data = array(
                'expire' => ($bill->expiration_date) ? $bill->expiration_date : '-',
                'status' => ($bill->expiration_date) ? 
                                (strtotime($bill->expiration_date) < time()) ?
                                    BILL_EXPIRED : BILL_PAID
                            : BILL_IDLE    
                
            );
            $out[$bill->id] = (object)$data;
        }
    }
    
    return $out;
}

function payment_monitor_text_bill_status($status_obj) {
    if (is_string( $status_obj) ) {
        $status = $status_obj;
    }
    if (is_object($status_obj)) {
        $status = $status_obj->status;
    }
    
    switch ($status) {
        case BILL_PAID:?>
            <span class="label label-success">Оплачена до <?php print $status_obj->expire; ?></span>
    <?php break;
        case BILL_EXPIRED:?>
            <span class="label label-danger">Квитанция просрочена</span>
    <?php break;
        case BILL_IDLE:?>
            <span class="label label-warning">Квитанция ожидает проверки</span>
    <?php break;
    }
}

function payment_monitor_upload_bill() {
    global $wpdb;
    global $payment_monitor_table_name;
    
    if (!session_id()) {
        session_start();
    }

    $_SESSION['payment_monitor_messages'] = array();
    
    add_filter('upload_dir', 'my_upload_dir');
    
    $uploadedfile = $_FILES['bill-image'];
    $upload_overrides = array('test_form' => false);
    
    add_filter('wp_handle_upload_prefilter', 'custom_upload_filter' );

    $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
    
    remove_filter('wp_handle_upload_prefilter', 'custom_upload_filter');
    remove_filter('upload_dir', 'my_upload_dir');
    
    if ( !isset($movefile['error']) ) {
        $wpdb->insert( $wpdb->prefix . $payment_monitor_table_name, array(
           'upload_time' => date('Y-m-d H:i:s'),
           'uid'         => get_current_user_id(),
           'file_url'   => $movefile['url'],
           'file_path'  => $movefile['file']
        ));
        $_SESSION['payment_monitor_messages']['success'] = 'Квитанция успешно загружена';
        $location = $_SERVER['HTTP_REFERER'];
        wp_safe_redirect($location);
    } else {
        print '<pre>' . print_r($movefile, true) . '</pre>';
        $_SESSION['payment_monitor_messages']['danger'] = 'При загрузке квитанции произошла ошибка';
    }
    
    //$location = $_SERVER['HTTP_REFERER'];
    //wp_safe_redirect($location);
}

function my_upload_dir($upload) {
    $upload['subdir'] = '/students' . $upload['subdir'];
    $upload['path'] = $upload['basedir'] . $upload['subdir'];
    $upload['url'] = $upload['baseurl'] . $upload['subdir'];
    return $upload;
}

function custom_upload_filter( $file ) {
    global $current_user;
    get_currentuserinfo();
    
    $file_ext = pathinfo($file['name'], PATHINFO_EXTENSION);
    $file['name'] = $current_user->user_login . '-' . uniqid() . '.' . $file_ext;
    
    return $file;
}

function payment_monitor_fetch_bill_url() {
    global $wpdb;
    global $payment_monitor_table_name;
    
    $id = filter_input(INPUT_GET, 'bill_id', FILTER_SANITIZE_NUMBER_INT);
    
    $query = "SELECT file_url FROM {$wpdb->prefix}$payment_monitor_table_name "
            . "WHERE id = $id";
    
    print $wpdb->get_var($query);
    exit;
}

function payment_monitor_change_bill_status($redirect = true) {
    global $wpdb;
    global $payment_monitor_table_name;
    
    $bill = filter_input(INPUT_POST, 'Bill', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    
    $data = array(
        'verification_date' => date('Y-m-d H:i:s'),
        'status' => $bill['status']
    );
    
    $expire = $bill['expire'];
    
    if ( !empty($expire) ) {
        $data['expiration_date'] = $expire;
    }
    
    $res = $wpdb->update( $wpdb->prefix.$payment_monitor_table_name, $data,
        array(
            'id' => $bill['id']
        )
    );
    
    if ($res === false) {
        _payment_monitor_set_message('Произошла ошибка', 'error ', true);
    } else {
        _payment_monitor_set_message('Все прошло успешно', 'updated ', true);
    }
    
    if ($redirect) {
        $location = $_SERVER['HTTP_REFERER'];
        wp_safe_redirect($location);
    }
}

function payment_monitor_remove_bill() {
    global $wpdb;
    global $payment_monitor_table_name;
    
    if (!session_id()) {
        session_start();
    }
    
    $ids = filter_input(INPUT_POST, 'bill_to_remove', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    
    $ids_for_query = "(". implode(',', $ids) . ")";
    
    $bill_files = $wpdb->get_col("SELECT file_path FROM {$wpdb->prefix}$payment_monitor_table_name "
            . "WHERE id IN $ids_for_query");
    
    foreach ($bill_files as $filepath) {
        unlink($filepath);
    }
    
    $result = $wpdb->query("DELETE FROM {$wpdb->prefix}$payment_monitor_table_name"
            . " WHERE id IN $ids_for_query");
    
    if ( $result === false || $result != count($ids) ) {
        $_SESSION['payment_monitor_messages']['danger'] = 'При удалении квитанций'
                . ' произошла ошибка';
    } else {
         $_SESSION['payment_monitor_messages']['success'] = 'Удаление успешно произведено';
    }
    
    $location = $_SERVER['HTTP_REFERER'];
    wp_safe_redirect($location);
}

function payment_monitor_menu() {
    add_menu_page( 'Payment Monitor Admin Panel', 'Payment Monitor', 'manage_options', 
            'payment_monitor_admin', 'payment_monitor_admin_page', 'dashicons-media-document');
}

function payment_monitor_admin_page() {
    if ( !current_user_can( 'manage_options' ) )  {
	wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
        
    if (!class_exists('WP_List_Table')) {
        require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
    }

    require_once dirname(__FILE__) . '/Apprentice_List_Table.php';
    
?>
<div class="wrap">
    <h2>Payment Monitor</h2>
    <form id="payment-change-status-form" method="POST" action="<?php print get_admin_url()."admin-post.php" ?>">
        <input type="hidden" name="Bill[status]" />
        <input type="hidden" name="action" value="change-bill-status" />
    </form>
    
    <form id="delete-user-form" method="POST" action="<?php print get_admin_url()."admin-post.php" ?>">
        <input type="hidden" name="action" value="delete-user" />
    </form>
    
    <div id="view-bill"></div>
    
    <form style="display:none" id="user_form" method="POST" action="<?php print get_admin_url()."admin-post.php" ?>">
        <input type="hidden" name="action" value="create-user" />
        <input type="hidden" name="User[role]" value="apprentice" />
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required">
                    <th><label for="user_login">Имя пользователя <span class="description">(*)</span></label></th>
                    <td>
                        <input type="text" name="User[user_login]" id="user_login"/>
                    </td>
                    <th>
                        <label for="email">
                            E-mail <span class="description">(*)
                            </span>
                        </label>
                    </th>
                    <td>
                        <input type="text" name="User[user_email]" id="email" />
                    </td>
                </tr>
                
                <tr class="form-field">
                    <th><label for="last_name">Фамилия </label></th>
                    <td>
                        <input type="text" name="User[last_name]" id="last_name" />
                    </td>
                    <th><label for="first_name">Имя </label></th>
                    <td>
                        <input type="text" name="User[first_name]" id="first_name" />
                    </td>
                </tr>
                
                <tr class="form-field">
                    <th><label for="user_login">Skype</label></th>
                    <td>
                        <input type="text" name="User[skype]" id="skype" />
                    </td>
                    <th><label for="pass">Пароль</label></th>
                    <td>
                        <input type="password" name="User[user_pass]" id="pass" />
                        <button class="button generate-password" title="Сгенерировать пароль">
                            <span class="dashicons dashicons-admin-network"></span>
                        </button>
                        <button class="button show-password" title="Показать пароль">
                            <span class="dashicons dashicons-visibility"></span>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <input type="hidden" name="Bill[status]" />
        <table class="bill-section">
            <tr>
                <td class="current-status"></td>
                <td><input class="form-payment-accept-datepicker" type="text" name="Bill[expire]" /></td>
                <td> 
                    <button class="status-buttons button payment-accept label-success">
                        <span class="dashicons dashicons-yes"></span>
                    </button>
                </td>
                <td>
                    <button class="status-buttons button payment-reject label-danger">
                        <span class="dashicons dashicons-no-alt"></span>
                    </button>
                </td>
                <td>
                    <button class="status-buttons button view-bill">
                        <span class="dashicons dashicons-search"></span>
                    </button>
                </td>
            </tr>
        </table>
        
        <p class="submit">
            <input type="submit" name="createuser" id="createusersub" 
                   class="button button-primary" value="Добавить нового пользователя">
            <button class="button button-cancel">Отмена</button>
        </p>
    </form>
    <!--<form id="wpse-list-table-form" method="POST">-->
<?php

    $page  = filter_input( INPUT_GET, 'page', FILTER_SANITIZE_STRIPPED );
    $paged = filter_input( INPUT_GET, 'paged', FILTER_SANITIZE_NUMBER_INT );

    printf( '<input type="hidden" name="page" value="%s" />', $page );
    printf( '<input type="hidden" name="paged" value="%d" />', $paged );
    
    $table = new Apprentice_List_Table();
    $table->prepare_items();
    $table->display(); ?>
    <!--</form>-->
</div><!--End of .wrap-->
<?php
    wp_enqueue_style(
        'payment_monitor_admin_css',
        plugins_url('css/payment_admin.css', __FILE__)
    );
    
    wp_enqueue_script(
        'payment_monitor_admin',
        plugins_url('js/payment_admin.js', __FILE__),
        array('jquery-ui-datepicker', 'jquery-ui-dialog'),
        false,
        true
    );
    
    wp_localize_script('payment_monitor_admin', 'BillStatus', array(
        'paid' => BILL_PAID,
        'rejected' => BILL_REJECTED,
        'idle' => BILL_IDLE
    ));
    //wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    
}

function payment_monitor_admin_notice() {
    $page = filter_input(INPUT_GET, 'page');

    session_start();
    
    if ( $page == 'payment_monitor_admin'
         && isset($_SESSION['payment_monitor_admin_messages']) ) {
        
        foreach ($_SESSION['payment_monitor_admin_messages'] as $class => $text) {?>
            <div class="<?php print $class?>">
                <p><?php print $text; ?></p>
            </div>
 <?php }
        unset($_SESSION['payment_monitor_admin_messages']);
    }
}
function payment_monitor_get_user_status($uid) {
    global $wpdb;
    global $payment_monitor_table_name;
    
    $table = $wpdb->prefix . $payment_monitor_table_name;
    
    $last_bill = null;
    $statuses = array(BILL_IDLE, BILL_PAID, BILL_REJECTED);
    
    for ($i = 0; $i < count($statuses); $i++) {
        $last_bill = $wpdb->get_row($wpdb->prepare(
            "
            SELECT `id`, `status`, `expiration_date` as expire
            FROM $table
            WHERE uid = %d
            AND status = %s
            ORDER BY upload_time DESC
            ", $uid, $statuses[$i]
        ));
        
        if ( !empty($last_bill) ) {
            break;
        }
    }
    
    if (!empty($last_bill)) {
        $object = new stdClass();

        $object->last_bill_id = $last_bill->id;
        $object->expire       = $last_bill->expire;

        if ( $last_bill->status == BILL_IDLE ) {
            $object->status = BILL_IDLE;
        } elseif ($last_bill->status != BILL_REJECTED) {
            $expire = new DateTime($last_bill->expire);

            if ($expire < new DateTime) {
                $object->status = BILL_EXPIRED;
            } else {
                $object->status = BILL_PAID;
            }
        } else {
            $object->status = BILL_REJECTED;
        }
        
        return $object;
    } else {
        return null;
    }
}

function payment_monitor_on_user_delete($uid) {
    global $wpdb;
    global $payment_monitor_table_name;
    
    $wpdb->query(
        $wpdb->prepare(
            "
            DELETE FROM {$wpdb->prefix}$payment_monitor_table_name
                WHERE uid = %d
            ", $uid
        )
    );
}

function payment_monitor_create_user() {
    $user_data = filter_input(INPUT_POST, 'User', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    
    if (!isset($user_data['ID']) ) {
        $user_data['user_pass'] = wp_hash_password($user_data['user_pass']);
        $user_id = wp_insert_user($user_data);
    } else {
        
        payment_monitor_change_bill_status(false);
        
        if ( empty($user_data['user_pass']) ) { 
            unset($user_data['user_pass']);
        } 
        
        $user_data['user_nicename'] = $user_data['display_name'] = $user_data['first_name'] . ' ' . $user_data['last_name'];
        update_user_meta($user_data['ID'], 'skype', $user_data['skype'], get_user_meta($user_data['ID'], 'skype', true));
        $user_id = wp_update_user($user_data);
//        print '<pre>' . print_r($user_data, true) . '</pre>';
//        print '<pre>' . print_r(get_userdata($user_id), true) . '</pre>';
//        exit;
    }
    
    if( !is_wp_error($user_id) ) {
        _payment_monitor_set_message(!isset($user_data['ID']) ? 'Пользователь создан успешно' : 'Пользователь изменен успешно', 'updated', true);
    } else {
        _payment_monitor_set_message('При создании пользователя '
                . 'произошла ошибка - ' . $user_id->get_error_message(), 'error', true);
    }
    
    $location = $_SERVER['HTTP_REFERER'];
    wp_safe_redirect($location);
}

function payment_monitor_delete_user() {
    $users_id = filter_input(INPUT_POST, 'users', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    foreach ($users_id as $user_id) {
        wp_delete_user($user_id);
    }
    
    _payment_monitor_set_message('Удаление прошло успешно', 'updated', true);
    
    $location = $_SERVER['HTTP_REFERER'];
    wp_safe_redirect($location);
}

function _payment_monitor_set_message($message, $level = 'success', $admin = false) {
    if (!session_id()) {
        session_start();
    }
    
    if ($admin) {
        $_SESSION['payment_monitor_admin_messages'] = array();
        $_SESSION['payment_monitor_admin_messages'][$level] = "<p>$message</p>";
    }
    
    $_SESSION['payment_monitor_messages'] = array();
    $_SESSION['payment_monitor_messages'][$level] = "<p>$message</p>";
}

function _payment_monitor_show_messages($admin = false) {
    if (!session_id()) {
        session_start();
    }
    
    $messages_key = (!$admin) ? 'payment_monitor_messages' : 'payment_monitor_admin_messages';
    
    if (isset($_SESSION[$messages_key])): 
        foreach ($_SESSION[$messages_key] as $key => $value): ?>
            <div class="alert alert-<?php print $key; ?>"><?php print $value; ?></div>
        <?php endforeach;
        unset($_SESSION[$messages_key]);
    endif;
}