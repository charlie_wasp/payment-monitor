<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Load_Bill_Widget
 *
 * @author charlie
 */
class Load_Bill_Widget extends WP_Widget {
    function __construct() {
        if (!session_id()) {
            session_start();
        }
        
        parent::__construct(
        'load_bill_widget',
        __('Upload Check Widget', 'load_bill_widget_domain'),
        array(
            'description' => __( 'Widget for uploading check scans by apprentices', 'load_bill_widget_domain' ), )
        );
        
    }
    
    public function widget($args, $instance) {
        $current_user = wp_get_current_user();
        //print '<pre>' . print_r($current_user, true) . '</pre>';
        if ( !in_array('apprentice', $current_user->roles) ) {
            return;
        }
        extract($args);
        
        
        wp_enqueue_script(
            'bill_widget_form_handler',
            plugins_url('js/payment_monitor_widget.js', __FILE__),
            array('jquery'),
            false,
            true
        );
        
        $title = apply_filters('widget_title', $instance['title'] );
        
        if (is_user_logged_in()) {
            echo $before_widget;
            
            $title = ($title) ? $title : __('Apprentice Menu', 'load_bill_widget_domain');
        
            $user_full_name = $current_user->user_firstname . ' ' . $current_user->user_lastname;
            $user_email = $current_user->user_email;
            $status_obj = payment_monitor_get_user_status($current_user->ID);
        ?>
        <div class="payment-monitor-widget">
            <?php print $before_title; ?><h3><?php print $title; ?></h3><?php print $after_title; ?>
            <?php if (isset($_SESSION['payment_monitor_messages'])): ?>
                <?php foreach ($_SESSION['payment_monitor_messages'] as $key => $value): ?>
                    <div class="alert alert-<?php print $key; ?>"><?php print $value; ?></div>
                <?php endforeach; ?>
            <?php unset($_SESSION['payment_monitor_messages']); endif; ?>
                    
            <p><?php payment_monitor_text_bill_status($status_obj); ?></p>
            <p><span class="glyphicon glyphicon-user"></span>&nbsp;<?php print $current_user->display_name;?></p>
            <p><span class="glyphicon glyphicon-envelope"></span>&nbsp<?php print $user_email;?></p>
            <p><b>S</b>&nbsp;&nbsp;<?php print get_user_meta($current_user->ID, 'skype', true);  ?></p>
            <p><a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit-apprentice-dialog">Изменить личные данные</a></p>
            <p><a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#bills-list">Cписок квитанций</a></p>
        </div>

        <div class="modal fade" id="edit-apprentice-dialog" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel" aria-hidden="true" style="display: none;">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	        <h4 class="modal-title" id="modalEditLabel">Изменение данных ученика</h4>
	      </div>
	      <div class="modal-body">
	      	<form id="edit-user-info-form" class="form-horizontal" method="POST" role="form" action="<?php print get_admin_url()."admin-post.php" ?>">
                    <input type='hidden' name='action' value='submit-form'>
                    <input type="hidden" name="ID" value="<?php print $current_user->ID; ?>">
	      	  <div class="form-group">
	      	    <label for="inputName" class="col-sm-3 control-label">Имя</label>
	      	    <div class="col-sm-9">
                        <input type="text" name="display_name" class="form-control" id="inputName" placeholder="Имя ученика" value="<?php print $current_user->display_name; ?>">
	      	    </div>
	      	  </div>
	      	  <div class="form-group">
	      	    <label for="inputEmail" class="col-sm-3 control-label">Email</label>
	      	    <div class="col-sm-9">
	      	      <input type="email" name="user_email" class="form-control" id="inputEmail" placeholder="Email ученика" value="<?php print $current_user->user_email; ?>">
	      	    </div>
	      	  </div>
	      	  <div class="form-group">
	      	    <label for="inputPassword" class="col-sm-3 control-label">Пароль&nbsp;
                        <button type="button" id="generatePassword" title="Генератор пароля">
                            <span class="glyphicon glyphicon-refresh"></span>
                        </button>
                    </label>
	      	    <div class="col-sm-9">
                        <div class="input-group">
                            <input class="form-control" type="password" name="user_pass" class="form-control" id="inputPassword" placeholder="Пароль">
                            <span class="input-group-btn">
                                <button class="btn btn-default look-password" type="button">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </button>
                            </span>
                        </div>
	      	    </div>
	      	  </div>
	      	  <div class="form-group">
	      	    <label for="inputConfirmPassword" class="col-sm-3 control-label">Повтор пароля</label>
	      	    <div class="col-sm-9">
	      	      <input type="password" name="confirm-password" class="form-control" id="inputConfirmPassword" placeholder="Пароль">
	      	    </div>
	      	  </div>
	      	  <div class="form-group">
	      	    <label for="inputSkype" class="col-sm-3 control-label">Логин Skype</label>
	      	    <div class="col-sm-9">
                        <input type="text" name="skype" class="form-control" id="inputSkype" placeholder="Логин Skype для связи с учеником" value="<?php print get_user_meta($current_user->ID, 'skype', TRUE); ?>">
	      	    </div>
	      	  </div>
	      	  <div class="form-group">
	      	    <div class="col-sm-offset-3 col-sm-9">
	      	      <input value="Сохранить" type="submit" class="btn btn-primary">
	      	      <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Отмена</button>
	      	    </div>
	      	  </div>
	      	</form>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div>
        
        <div class="modal fade in" id="bills-list" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="listBillsModalLabel">Список квитанций</h4>
                    </div>
                    <div class="modal-body">
                        <form id="remove-bill-form" method="POST" action="<?php print get_admin_url()."admin-post.php" ?>" class="pull-left form-inline" role="form">
                            <input type="hidden" name="action" value="remove-bill">
                            <a data-toggle="modal" data-target="#confirm-delete" class="remove-bill-form-submit btn btn-danger">Удалить</a>
                        </form>
                        <form method="POST" action="<?php print get_admin_url()."admin-post.php" ?>" enctype="multipart/form-data" class="form-inline" role="form" style="margin-bottom:15px;">
                            <input type="hidden" name="action" value="upload-bill">
                            <div class="form-group">
                                <input name="bill-image" type="file" class="form-control">
                                <input type="submit" disabled value="Загрузить" class="btn btn-default">
                            </div>
                        </form>
                        <?php $bills = payment_monitor_get_user_bills($current_user->ID); ?>
                        <?php if ( count($bills) == 0 ): ?>
                        У вас еще нет загруженных квитанций
                        <?php else: ?>
                        <table id="" class="table table-striped table-hover" style="margin:0;">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>#</th>
                                    <th>Срок</th>
                                    <th>Статус</th>
                                    <th>Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; foreach ($bills as $id => $data) : ?>
                                <tr data-bill-id="<?php print $id; ?>">
                                    <td><input form="remove-bill-form" name="bill_to_remove[]" type="checkbox" value="<?php print $id; ?>"></td>
                                    <td><?php print $i; ?></td>
                                    <td><?php print ($data->expire); ?></td>
                                    <td><?php payment_monitor_text_bill_status($data); ?></td>
                                    <td>
                                        <a href="#" data-toggle="modal" class="view-bill-link" data-target="#modalPayment">
                                            Просмотр
                                        </a> | 
                                        <a class="remove-bill-link" data-toggle="modal" data-target="#confirm-delete">
                                            Удалить
                                        </a>
                                    </td>
                                </tr>
                                <?php $i++; endforeach; ?>
                            </tbody>
                        </table>
                        <?php endif; ?>
                    </div>
                    <div class="modal-footer" style="margin:0;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade in" id="modalPayment" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="modalPaymentLabel">Квитанция об оплате</h4>
                    </div>
                <div class="modal-body text-center">
                </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div>

        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                       <h4 class="modal-title">Удаление квитанции</h4> 
                    </div>
                    <div class="modal-body">
                
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                        <a href="#" class="btn btn-danger danger">Удалить</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        echo $after_widget;
        }
    }
    
    // Widget Backend
    public function form( $instance ) {
        if ( isset( $instance['title'] ) ) {
            $title = $instance['title'];
        } else {
            $title = __( 'New title', 'load_bill_widget_domain' );
        }
    ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( ($title) ? $title : __('Apprentice Menu', 'load_bill_widget_domain') ); ?>" />
            
        </p>
    <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = (!empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
} // Class wpb_widget ends here

